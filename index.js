class Burger {
  constructor(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.burgerSize = {
      small: {
        price: 50,
        calories: 20
      },
      large: {
        price: 100,
        calories: 40
      }
    };
    this.stuffingTypes = {
      cheese: {
        calories: 20,
        price: 10
      },
      salad: {
        calories: 5,
        price: 20
      },
      potato: {
        calories: 10,
        price: 15
      }
    };
  }
  checkASize() {
    return this.size;
  }
  findStuffing() {
    return this.stuffing;
  }
  cheeseStaffing() {
    return (this.stuffing = "cheese");
  }
  saladStaffing() {
    return (this.stuffing = "salad");
  }
  potatoStaffing() {
    return (this.stuffing = "potato");
  }

  calculatePrice() {
    return (
      this.stuffingTypes[this.findStuffing()]["price"] +
      this.burgerSize[this.checkASize()]["price"]
    );
  }
  calculateCalories() {
    return (
      this.stuffingTypes[this.findStuffing()]["calories"] +
      this.burgerSize[this.checkASize()]["calories"]
    );
  }
  chooseSmallBurger() {
    return (this.size = "small");
  }

  chooseLargeBurger() {
    return (this.size = "large");
  }
}

class StandartBurger extends Burger {
  constructor(size, stuffing, dressing) {
    super(size, stuffing);
    this.dressing = dressing;
    this.dressingTypes = {
      sauce: {
        calories: 0,
        price: 15
      },
      mayo: {
        calories: 5,
        price: 20
      },
      no: {
        calories: 0,
        price: 0
      }
    };
  }
  findDressing() {
    return this.dressing;
  }
  zeroDressing() {
    return (this.dressing = "no");
  }
  sauceDressing() {
    return (this.dressing = "sauce");
  }
  mayoDressing() {
    return (this.dressing = "mayo");
  }
  calculatePrice() {
    return (
      super.calculatePrice() + this.dressingTypes[this.findDressing()]["price"]
    );
  }
  calculateCalories() {
    return (
      super.calculateCalories() +
      this.dressingTypes[this.findDressing()]["calories"]
    );
  }
}

let forNastya = new StandartBurger("small", "salad", "sauce");
console.log(forNastya.findDressing());
console.log(forNastya.findStuffing());
console.log(forNastya.chooseLargeBurger());
console.log(forNastya.findDressing());
console.log(forNastya.zeroDressing());
console.log(forNastya.findDressing());
console.log(forNastya.calculatePrice());
console.log(forNastya.calculateCalories());
